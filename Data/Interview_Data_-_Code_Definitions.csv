UES Framework,Focused Attention,Feeling absorbed in the interaction and losing track of time.
,Perceived Usability,Negative affect experienced as a result of the interaction and the degree of control and effort expended.
,Aesthetic Appeal,Attractiveness and visual (and sonic) appeal of the instrument.
,Novelty,Curiosity and interest in the interactive task.
,Felt Involvement,The sense of being “drawn in” and having fun.
,Endurability,Overall success of the interactions and users’ willingness to recommend an application to others or engage with it in future.
,Reason for Disengagement,Comments that describe a reason for not engaging/engaging less with the T-Stick.
MPX-Q Framework,Experienced Freedom & Possibilities,Related to exploring new things in music through the help of a musical instrument. Corresponding items converge to an underlying motive of finding new ways to develop musicianship and musical expressivity.
,Perceived Control & Comfort,Describes musical controllability and ergonomic aspects which contribute to the perceived comfort of a musical instrument.
,"Perceived Stability, Sound Quality, & Aesthetics","Includes classic instrument quality notions that are found in the materials, the sound, or the visual appearance of an instrument."
Technical Factors,Hardware,Comments related to the physical object and the sensors used in the T-Stick.
,Mappings,Comments related to the mapping between physical gesture and sound.
,Gestures,Comments related to the physical actions used to control the sound synthesis parameters.
,Sound Synthesis,Comments related to the sound quality or available parameters.
,Feedback,Comments related to the feedback provided by the instrument.
,Perceived Glitches/Bugs,Comment related to technical issues encountered that users attribute to glitches or bugs
,Suggestions for Improvement,"Participants’ ideas for how the hardware, mappings, gestures, sound synthesis, or feedback could be improved."
Factors of Interest,"Quality: Aesthetic, Sonic, & Mappings","Comments related to sound quality, visual attractiveness, mapping effectiveness, or other visceral attributes. Overlap with Aesthetic Appeal and Perceived Stability, Sound Quality, & Aesthetics."
,Absorption & Flow,Comments related to feeling immersed in the interactions with the T-Stick. Overlap with Focused Attention and Felt Involvement.
,Control & Intuitiveness,"Comments related to the level of control individuals feel in their interactions with the T-Stick, as well as the extent to which the control seems intuitive or natural. "
,Challenges & Difficulties,Comments related to challenges experiences in individuals’ interactions with the T-Stick.
,"Motivation, Fun, & Reward","Comments related to enjoyable and rewarding aspects of individuals’ interactions with the T-Stick, as well as their motivation to engage with the instrument."
Affective Response,Frustration,Comments that reflect affective responses of frustration.
,Excitement/Reward,Comments that reflect affective responses of excitement or reward.
,Confusion,Comments that reflect affective responses of confusion.
,Breakthroughs,Comments related to breakthroughs that users experienced in the course of their interaction with the instrument
Methodological Factors,Task,Comments related to the tasks participants were asked to perform.
,Instructions/Demonstrations,Comments related to the task video demonstrations or embedded written instructions provided for participants.
,Practice Time,Comments related to the amount and distribution of practice time.
,Environment,Comments related to the physical environment in which the research was conducted.
Personal Factors,Background,Comments relating to participants’ previous life experiences and knowledge (both music-related and non-music-related).
,Practice/Preferences,Preferences or typical behaviours related to participants’ individual musical performance practice.
,Acoustic Instrument Comparison,Comments that compare the T-Stick or other DMIs with acoustic instruments.
,Motor Factors,"Comments relating to movement, proprioception, or pain."
,Philosophical,Comments related to higher level abstract concepts in terms of musical practice
,Creativity,Comments related to how the instrument inspires users creatively or how they might incorporate it into the context of their creative practice
,Audience/External Appearance,Comments related to how users imagine the instrument might appear to other observers or audience members
Temporal Factors,Long-Term Adoption,Comments relating to participants intentions or ideas for using the T-Stick in the future.
,Appropriation ,"Comments relating to how participants make technology their own and situate it within their personal knowledge, practice, and taste. (Bar, Weber, & Pisani, 2016)"
,Embodiment,"Comments that reflect the development of an intimate and engaging relationship between participants and the T-Stick. (Fels, 2004)"
,Learning,Comments related to the acquisition of knowledge and skills relevant to playing the T-Stick.
,Changes Over Time,Comments that describe an aspect of participants’ experience with the T-Stick that has shifted over time.
Interaction Factors,Affordances/Constraints,Comments related to basic conceptions of affordances (suggest range of possibilities) and constraints (limit alternatives)
,Flexibility/Possibilities,Comments related to the range of musical and performance possibilities offered by the instrument and how these possibilities might be leveraged by users in different musical contexts
,Limitations,Comments related to aspects of the instrument that users interpreted as limiting the musical and performance possibilities offered by the instrument
,Error Recovery,Comments related to how users are able to overcome errors encountered when interacting with the instrument
,Transparency/Understanding,Comments related to users’ ease in understanding the relationship between gestural input and sonic output
,Fragility/Stability,"Comments related to the fragility, stability, durability, and robustness of the instrument in the context of musical practice"
,Responsiveness/Consistency,Comments related to the degree to which users find that the instrument responds to their gestures in a timely and consistent manner
,Exploration,Comments related to the extent to which users can explore a piece of music technology and how they go about doing so
,Discoverability,Comments related to features or affordances of the T-Stick that participants’ discovered of their own accord.